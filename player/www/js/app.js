(function(){
  var template1 = Handlebars.compile($("#tpt1").html());
  var text = '' ;
  var data = '' ;
  console.log("tpt1") ;


//First execution
  setTimeout(function(){
    makeRequest('GET', 'assets/json/test.json', function (err, datums) {
      if (err) { throw err; }
      text = datums ;
      data = JSON.parse(text);
      var rand = Math.floor(Math.random() * 6);
      render_template1(template1, data.schedule[rand]) ;
    });
  }, 0);

//Get data every 60"
  window.setInterval(function(){
    console.log("new request") ;
    makeRequest('GET', 'assets/json/test.json', function (err, datums) {
      if (err) { throw err; }
      text = datums ;
      data = JSON.parse(text);
    });
  }, 60000);

  //Change property data every 10"
  window.setInterval(function(){
    var rand = Math.floor(Math.random() * 4);
    console.log("random:" + rand) ;
    $('body').removeClass('fade-out');
    render_template1(template1, data.schedule[rand]) ;
    fade_timeout(9000) ;
  }, 10000);


  function render_template1(templateScript, context) {
    console.log("rendering template1") ;
    var html = templateScript(context);
    $(document.body).html(html);
    $( '#slideshow' ).cycle();
  }

  function makeRequest (method, url, done) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onload = function () {
      done(null, xhr.response);
    };
    xhr.onerror = function () {
      done(xhr.response);
    };
    xhr.send();
  }

  function fade_timeout(millis) {
    setTimeout(function(){
      document.body.className += ' fade-out';
    }, millis);
  }

  })();
